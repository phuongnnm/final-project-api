import { HttpException, HttpStatus } from "@nestjs/common";

export default class ValidationError extends HttpException {
    constructor(errors) {
        let errKeys = Object.keys(errors[0].constraints);
        let ret = errors[0].constraints[errKeys[0]];
        super(ret, HttpStatus.BAD_REQUEST);
    }
} 