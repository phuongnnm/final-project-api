import { Injectable } from "@nestjs/common";
import Seller from "../entity/seller.entity";
import { UploadService } from "../upload/upload.service";


@Injectable()
export class SellerUpload {
    constructor(private readonly uploadService: UploadService) { }
    getExtensionType = (fileName) => fileName.substring(fileName.lastIndexOf(".") + 1, fileName.length).toLowerCase();
    
    async deleteExistedFolder(filepath) {
        this.uploadService.deleteFolderIfExisted(filepath)
    }
    async uploadContents(file, folderPath, sellerId) {
        const seller = await Seller.createQueryBuilder('seller')
            .select(['seller.id'])
            .where(`seller.id = '${sellerId}'`)
            .getOne();
        const { mimetype } = await file
        try {
            let filePath = `${folderPath}/${seller.id}/avatar.jpeg`;
            // await this.uploadService.deleteFolderIfExisted(`${folderPath}/${seller.id}`);
            return this.uploadService.uploadFile(filePath, file, 'public-read');      
        } 
        catch (err) {
            return { status: 1, message: err.message }
        }
    }


}