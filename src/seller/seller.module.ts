import { Module } from '@nestjs/common';
import { UploadService } from 'src/upload/upload.service';
import { SellerResolver } from '../graphql/resolvers/seller';
import { SellerService } from './seller.service';
import { SellerUpload } from './sellerUpload.service';

@Module({
  providers: [SellerService, SellerResolver, UploadService, SellerUpload]
})
export class SellerModule {}
