import { Injectable } from '@nestjs/common';
import Service from 'src/entity/service.entity';
import Seller from '../entity/seller.entity';
import { slugify } from '../util/constants';

@Injectable()
export class SellerService {
    async addPhotoUrlsToSeller(id, serviceId, avaUrl, portfolioPhotoUrls, serviceUrls) {
        let seller = Seller.create({
            id,
            avatarUrl: avaUrl,
            portfolioPhotoUrls: portfolioPhotoUrls,
        })

        let service = Service.create({
            id: serviceId,
            servicePhotoUrls: serviceUrls
        })
        await service.save()
        return seller.save()
    }

    async getSellerByUserid(userId) {
        const ret = await Seller.createQueryBuilder('seller')
                    .leftJoin('seller.user', 'user')
                    .leftJoinAndSelect('seller.services', 'services')
                    .where(`user.id = '${userId}'`)
                    .getOne()
        return ret
    }

    async searchSellersByKeyword(keyword) {
        let slugifiedKey = slugify(keyword)
        const ret = await Seller.createQueryBuilder('seller')
            .leftJoinAndSelect('seller.services', 'services')
            .leftJoinAndSelect('services.category', 'category')
            .addSelect(`word_similarity('${slugifiedKey}', unaccent(seller.firstName))`, 'firstname')
            .addSelect(`word_similarity('${slugifiedKey}', unaccent(seller.lastName))`, 'lastname')
            .where(`word_similarity('${slugifiedKey}', unaccent(seller.firstName)) > 0.6`)
            .orWhere(`word_similarity('${slugifiedKey}', unaccent(seller.lastName)) > 0.6`)
            .orderBy('lastname', 'DESC')
            .addOrderBy('firstname', "DESC")
            .take(15)
            .getMany()

        console.log("ret");
        console.log(ret);
        return ret
    }
}
