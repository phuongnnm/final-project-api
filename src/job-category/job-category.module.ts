import { Module } from '@nestjs/common';
import { JobCategoryResolver } from '../graphql/resolvers/jobCategory';
import { JobCategoryService } from './job-category.service';

@Module({
  providers: [JobCategoryService, JobCategoryResolver]
})
export class JobCategoryModule {}
