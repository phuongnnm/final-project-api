import { Module } from '@nestjs/common';
import { ServiceResolver } from 'src/graphql/resolvers/service';
import { ServiceService } from './service.service';

@Module({
  providers: [ServiceService, ServiceResolver]
})
export class ServiceModule {}
