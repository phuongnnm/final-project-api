import { Injectable } from '@nestjs/common';
import Service from 'src/entity/service.entity';
import { slugify } from 'src/util/constants';

@Injectable()
export class ServiceService {
    async searchServiceByKeyword(keyword) {
        let slugifiedKey = slugify(keyword)
        let ret = await Service.createQueryBuilder('service')
            .leftJoinAndSelect('service.category', 'category')
            .leftJoinAndSelect('service.packages', 'packages')
            .leftJoin('service.seller', 'seller')
            .addSelect('seller.avatarUrl')
            .addSelect('seller.firstName')
            .addSelect('seller.lastName')
            .addSelect(`word_similarity('${slugifiedKey}', unaccent(service.serviceDescription))`, 'descriptionsimi')
            .addSelect(`word_similarity('${slugifiedKey}', unaccent(category."categoryName"))`, 'catenamesimi')
            .where(`word_similarity('${slugifiedKey}', unaccent(service.serviceDescription)) > 0.6`)
            .orWhere(`word_similarity('${slugifiedKey}', unaccent(category."categoryName")) > 0.6`)
            .orderBy('catenamesimi', 'DESC')
            .addOrderBy('descriptionsimi', 'DESC')
            .addOrderBy('packages.price', 'ASC')
            .take(15)
            .getMany()

        console.log('==> ret');
        console.log(ret)

        return ret
    }

    async getServiceById(id) {
        let ret = await Service.createQueryBuilder('service')
            .leftJoinAndSelect('service.seller', 'seller')
            .leftJoinAndSelect('service.packages', 'packages')
            .leftJoin('seller.user', 'user')
            .addSelect('user.id')
            .where(`service.id = '${id}'`)
            .getOne()
        return ret
    }
}
