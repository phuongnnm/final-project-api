import { Column, Entity, ManyToOne } from "typeorm";
import { PackageType } from "../models/packageType";
import Base from "./base.entity";
import Service from "./service.entity";

@Entity()
export default class ServicePackage extends Base {
    @Column({ type: 'enum', enum: PackageType })
    packageType: PackageType;

    @Column('text', { default: '{}', array: true })
    deliverables: String[]

    @Column({ nullable: false, type: 'decimal' })
    price: number;

    @Column({ nullable: true, type: 'decimal' })
    quantity: number;

    @Column({ nullable: true })
    unit: String 
    
    @Column({ nullable: true })
    description: String

    @ManyToOne(type => Service, service => service.packages)
    service: Service;
}