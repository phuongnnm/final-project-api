import { PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn, BaseEntity, BeforeInsert, BeforeUpdate } from 'typeorm';
import { validate } from "class-validator";
import ValidationError from '../errors/validationError';

export default abstract class Base extends BaseEntity {
    @PrimaryGeneratedColumn('uuid')
    id: String;

    @CreateDateColumn()
    createdAt: Date;

    @UpdateDateColumn()
    updatedAt: Date;

    @BeforeInsert()
    @BeforeUpdate()
    async validate() {
        const errors = await validate(this, { skipMissingProperties: true });
        if (errors.length > 0) {
            throw new ValidationError(errors);
        }
    }
}