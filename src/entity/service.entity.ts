import { Column, Entity, ManyToOne, OneToMany, OneToOne } from "typeorm";
import Base from "./base.entity";
import JobCategory from "./jobCategory.entity";
import Seller from "./seller.entity";
import ServicePackage from "./servicePackage.entity";

@Entity()
export default class Service extends Base {
    @ManyToOne(type => Seller, seller => seller.services)
    seller: Seller

    @OneToMany(type => ServicePackage, packages => packages.service)
    packages: ServicePackage[]

    @ManyToOne(type => JobCategory, category => category.services) 
    category: JobCategory

    @Column()
    serviceDescription: String

    @Column({ nullable: true })
    serviceInfo: String

    @Column('text', { default: '{}', array: true })
    servicePhotoUrls: String[]
}