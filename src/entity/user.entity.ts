import { Column, Entity, OneToMany, BeforeInsert, ManyToMany, JoinTable, AfterLoad, BeforeUpdate, Unique } from 'typeorm';
import * as crypto from 'crypto';
import { RegisterType } from 'src/models/registerType';
import { MaxLength, MinLength, IsEmail } from 'class-validator';
import { validatedMessage } from '../util/constants';
import { Role } from 'src/models/roles';
import Base from './base.entity';

@Entity()
export default class User extends Base {
    @Column('enum', { default: Role.USER, enum: Role })
    role: Role;

    @Column()
    @IsEmail({}, { message: validatedMessage.email })
    @MaxLength(255, { message: validatedMessage.maxLength() })
    email: string;

    @Column({ nullable: true })
    displayName: string

    @BeforeInsert()
    @BeforeUpdate()
    hashPassword() {
        if (this.password) {
            this.passwordSalt = crypto.randomBytes(128).toString('base64');
            this.password = crypto
                .createHmac('sha256', this.password)
                .update(this.passwordSalt)
                .digest('hex');
        }
    }

    @Column({ select: false, nullable: true })
    @MinLength(6, { message: validatedMessage.minLength() })
    @MaxLength(40, { message: validatedMessage.maxLength() })
    password: string;

    @Column({ select: false, nullable: true })
    @MinLength(6, { message: validatedMessage.minLength() })
    @MaxLength(255, { message: validatedMessage.maxLength() })
    passwordSalt: string;
    
    @Column({ nullable: true })
    avatarUrl: string;

    @Column({ type: 'enum', enum: RegisterType, default: RegisterType.DEFAULT })
    registerType: RegisterType;
}