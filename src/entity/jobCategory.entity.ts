import { Column, Entity, OneToMany } from "typeorm";
import Base from "./base.entity";
import Service from "./service.entity";

@Entity()
export default class JobCategory extends Base {
    @Column()
    categoryName: String

    @Column()
    categoryDescription: String

    @OneToMany(type => Service, services => services.category)
    services: Service[]
}