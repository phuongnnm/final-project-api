import { AfterLoad, Column, Entity, JoinColumn, OneToMany, OneToOne } from "typeorm";
import Base from "./base.entity";
import Service from "./service.entity";
import User from "./user.entity";

@Entity()
export default class Seller extends Base {
    @OneToOne(type => User)
    @JoinColumn()
    user: User

    @Column()
    description: String

    @Column({ nullable: true })
    avatarUrl: String

    @Column('text', { default: '{}', array: true })
    portfolioPhotoUrls: String[]

    @OneToMany(type => Service, service => service.seller)
    services: Service[]

    @Column({ nullable: true })
    firstName: string;

    @Column({ nullable: true })
    lastName: string;

    fullName: string;

    @AfterLoad()
    assignFormattedProperties() {
        if (this.firstName || this.lastName) {
            this.fullName = `${this.lastName || ''} ${this.firstName || ''}`;
        }
    }
}