import { UseGuards } from "@nestjs/common";
import { Args, Mutation, Resolver, Query } from "@nestjs/graphql";
import User from "../../entity/user.entity";
import { GraphAuthGuard } from "../../user/graphql-auth.guard";
import { SetRoles } from "../../user/roles.decorator";
import { RolesGuard } from "../../user/roles.guard";
import { UserService } from "../../user/user.service";
import { Role } from "../../models/roles"
import { CurrentUser } from "../../user/user-decorator";

@Resolver('User')
export class UserResolver {
    constructor(private readonly userService: UserService) { }

    @Mutation()
    @SetRoles(Role.ADMIN)
    @UseGuards(GraphAuthGuard, RolesGuard)
    async deleteUser(@Args('id') id) {
        return this.userService.deleteUser(id);
    }

    @Mutation()
    @SetRoles(Role.ADMIN)
    @UseGuards(GraphAuthGuard, RolesGuard)
    async updateUser(
        @Args('id') id,
        @Args('displayName') displayName,
        @Args('role') role: Role,
    ) {
        return this.userService.updateUser(User.create({ id, displayName, role }));
    }

    @Mutation()
    @SetRoles(Role.ADMIN)
    async createUser(@Args('email') email,
        @Args('password') password,
        @Args('displayName') displayName,
        @Args('role') role) {
        const userInfo = User.create({
            email,
            password,
            displayName,
            role,
        })
        const response = await this.registerUser(userInfo)

        if (response.status === 0) {
            return response.userData
        } else {
            throw new Error(response.message)
        }
    }

    @Query()
    @SetRoles(Role.USER)
    @UseGuards(GraphAuthGuard, RolesGuard)
    async allUsers() {
        return User.find();
    }

    @Query()
    @SetRoles(Role.ADMIN)
    @UseGuards(GraphAuthGuard, RolesGuard)
    async _allUsersMeta() {
        const ret = await this.allUsers();

        return {
            count: ret.length,
        };
    }

    @Query()
    async User(@Args('id') id) {
        return User.findOne(id)
    }

    @Mutation()
    login(@Args('user') user: User) {
        console.log("LOGING");
        
        return this.userService.login(user.email, user.password, false);
    }

    @Mutation()
    loginAdmin(@Args('user') user: User) {
        return this.userService.login(user.email, user.password, true);
    }

    @Mutation()
    async registerUser(@Args('user') userInfo) {
        console.log("REGISTER");
        
        const isExisted = await this.userService.emailIsExisted(userInfo.email);
        if (isExisted) {
            return { status: 1, message: 'This email is existed', userData: null };
        }
        return this.userService.createUser(userInfo).then(async result => {
            return { status: 0, message: 'Successfully registered', userData: result };
        }).catch(err => {
            const msg = err.detail && `${err.detail.substring(err.detail.indexOf('=') + 2, err.detail.lastIndexOf(')'))} đã tồn tại`;
            return { status: 2, message: msg || err, userData: null };
        });
    }

}