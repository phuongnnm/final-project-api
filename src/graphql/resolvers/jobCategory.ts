import { UseGuards } from "@nestjs/common";
import { Resolver, Query } from "@nestjs/graphql";
import { Role } from "src/models/roles";
import { GraphAuthGuard } from "src/user/graphql-auth.guard";
import { SetRoles } from "src/user/roles.decorator";
import { RolesGuard } from "src/user/roles.guard";
import JobCategory from "../../entity/jobCategory.entity";
import { JobCategoryService } from "../../job-category/job-category.service";

@Resolver('JobCategory')
export class JobCategoryResolver {
    constructor(private readonly categoryService: JobCategoryService) { }

    @Query()
    // @SetRoles(Role.ADMIN)
    // @UseGuards(GraphAuthGuard, RolesGuard)
    async allJobCategories() {
        return JobCategory.find()
    }
}