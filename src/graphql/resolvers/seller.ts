import { UseGuards } from "@nestjs/common";
import { Args, Mutation, Resolver, Query } from "@nestjs/graphql";
import JobCategory from "../../entity/jobCategory.entity";
import Seller from "../../entity/seller.entity";
import Service from "../../entity/service.entity";
import ServicePackage from "../../entity/servicePackage.entity";
import { GraphAuthGuard } from "../../user/graphql-auth.guard";
import { SetRoles } from "../../user/roles.decorator";
import { RolesGuard } from "../../user/roles.guard";
import { Role } from "../../models/roles"
import { getConnection } from "typeorm";
import { SellerService } from "../../seller/seller.service";
import { SellerUpload } from "../../seller/sellerUpload.service";

@Resolver('Seller')
export class SellerResolver {
    constructor(
        private readonly sellerService: SellerService, 
        private readonly sellerUpload: SellerUpload
        ) { }

    @Query()
    async searchSellersByKeyword(@Args('keyWord') keyWord) {
        return this.sellerService.searchSellersByKeyword(keyWord)
    }

    @Mutation()
    @SetRoles(Role.USER)
    @UseGuards(GraphAuthGuard, RolesGuard)
    async deleteExistedSellerFolder(@Args('filepath') filepath) {
        await this.sellerUpload.deleteExistedFolder(filepath)
        return { message: "MESSAGE FROM API" }
    }

    @Query()
    async getSellerByUserid(@Args('id') userId) {
        return this.sellerService.getSellerByUserid(userId)
    }

    @Query()
    @SetRoles(Role.USER)
    @UseGuards(GraphAuthGuard, RolesGuard)
    async allSellers() {
        return Seller.find()
    }

    @Query() 
    @SetRoles(Role.ADMIN)
    @UseGuards(GraphAuthGuard, RolesGuard)
    async _allSellersMeta() {
        const ret = await this.allSellers()
        return {
            count: ret.length,
        }
    }

    @Query() 
    async Seller(@Args('id') id) {
        return Seller.findOne(id)
    }

    @Mutation()
    async addPhotoUrlsToSeller(@Args('id') sellerId, @Args('serviceId') serviceId, @Args('avatarUrl') avaUrl, @Args('portfolioUrls') portfolioUrls, @Args('serviceUrls') serviceUrls) {
        return this.sellerService.addPhotoUrlsToSeller(sellerId, serviceId, avaUrl, portfolioUrls, serviceUrls)
    }

    @Mutation()
    async createSellerProfile(@Args('seller') seller, @Args('userId') userId) {
        console.log('==> seller');
        console.log(seller)
        const sellerData = await getConnection().transaction(async transactionalEntityManager => {
            const sellerBasicInfo = ({ service, ...seller }) => seller;
            const createdSeller = await transactionalEntityManager.save(
                Seller.create({ ...sellerBasicInfo(seller), user: { id: userId } }),
            );

            const { servicePackages, category, ...serviceWithoutPackages } = seller.service

            const createdService: any = await transactionalEntityManager.save(
                Service.create({
                    category: JobCategory.create({ id: category.id }),
                    seller: Seller.create({ id: createdSeller.id }),
                    ...serviceWithoutPackages,
                })
            );

            console.log('==> createdService');
            console.log(createdService)

            const servicepackages = servicePackages.map(element => 
                {
                    console.log('==> element');
                    console.log(element)
                    console.log("Price")
                    console.log(element.price)
                    return ServicePackage.create({ 
                        ...element,
                        service: Service.create({ id: createdService.id}) })
                })

            await transactionalEntityManager.save(servicepackages)

            // const createdService = await()
            return createdSeller
        })

        return sellerData
    }
}