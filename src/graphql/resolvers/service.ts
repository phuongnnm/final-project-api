import { Resolver, Query, Args } from "@nestjs/graphql";
import { ServiceService } from "../../service/service.service";

@Resolver('Service')
export class ServiceResolver {
    constructor(private readonly serviceService: ServiceService) {}

    @Query()
    async searchServiceByKeyword(@Args('keyWord') keyword) {
        return this.serviceService.searchServiceByKeyword(keyword)
    }

    @Query()
    async Service(@Args('id') id) {
        return this.serviceService.getServiceById(id)
    }
}