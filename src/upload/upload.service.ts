import { Injectable } from '@nestjs/common';
import { ConfigService } from '../config/config.service';
var AWS = require("aws-sdk");

@Injectable()
export class UploadService {
    private s3;
    private bucketName;

    constructor(private readonly configService: ConfigService) {
        let configs = {
            accessKeyId: this.configService.get("AWS_KEYID"),
            secretAccessKey: this.configService.get("AWS_ACCESSKEY"),
            region: 'ap-southeast-1',
            apiVersion: '2006-03-01'
        }

        this.bucketName = `${this.configService.get("AWS_BUCKETNAME")}`;
        this.s3 = new AWS.S3(configs);
    }

    async deleteExistedFile(filePath) {
        let params = {
            Bucket: this.bucketName,
            Key: filePath
        };
        
        return new Promise((resolve, reject) => {
            this.s3.deleteObjects(params, (listErr, listData) => {
                if (listErr) reject(listErr);
                else {
                    resolve(listData);
                }
            });
        })
    }

    async deleteFolderIfExisted(folderPath): Promise<any> {
        let listObjectParams = {
            Bucket: this.bucketName,
            Prefix: folderPath
        };

        return new Promise<void>((resolve, reject) => {
            this.s3.listObjects(listObjectParams, (err, data) => {
                if (err) reject(err);

                if(data.Contents.length === 0) resolve();

                let deletedObjectParams = { Bucket: listObjectParams.Bucket, Delete: { Objects: [] } };

                data.Contents.forEach((content) => {
                    deletedObjectParams.Delete.Objects.push({ Key: content.Key });
                });

                this.s3.deleteObjects(deletedObjectParams, (listErr, listData) => {
                    if (listErr) reject(listErr);
                    else {
                        resolve(listData);
                    }
                });
            });
        });
    }

    async uploadFile(filePath, fileContent, accessControl): Promise<any> {
        let { createReadStream } = await fileContent;
        let stream = createReadStream();
        const params = {
            Bucket: this.bucketName,
            Key: filePath,
            Body: stream,
            ACL: accessControl
        }

        return new Promise((resolve, reject) => {
            this.s3.upload(params, (err, data) => {
                if (err) {
                    reject(filePath);
                }

                if (data) {
                    resolve(data.Location);
                }
            });
        });
    }
}
