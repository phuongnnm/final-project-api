import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UserModule } from './user/user.module';
import { ConfigModule } from './config/config.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { GraphQLModule } from '@nestjs/graphql';
import { GraphQLUpload } from 'graphql-upload';
import { Connection } from 'typeorm';
import { AuthModule } from './user/auth/auth.module';
import { SellerModule } from './seller/seller.module';
import { UploadService } from './upload/upload.service';
import { JobCategoryModule } from './job-category/job-category.module';
import { ServiceModule } from './service/service.module';

@Module({
  imports: [
    GraphQLModule.forRoot({
      context: ({ req }) => ({ req }),
      typeDefs: /* GraphQL */ `
        scalar Upload
      `,
      resolvers: { Upload: GraphQLUpload },
      uploads: {
        maxFileSize: 2000000,
        maxFiles: 5,
      },
      typePaths: ['./**/*.graphql'],
      installSubscriptionHandlers: true,
      introspection: true,
    }),
    TypeOrmModule.forRoot(),
    AuthModule,
    ConfigModule,
    UserModule,
    SellerModule,
    JobCategoryModule,
    ServiceModule, 
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  constructor(private readonly connection: Connection) { }
}
