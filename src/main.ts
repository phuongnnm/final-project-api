import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import * as winston from 'winston';
import DailyRotateFile = require('winston-daily-rotate-file');

import {
  utilities as nestWinstonModuleUtilities,
  WinstonModule,
} from 'nest-winston';

declare const module: any;
async function bootstrap() {
  const app = await NestFactory.create(AppModule, {
    logger: WinstonModule.createLogger({
      transports: [
        new winston.transports.Console({
          format: winston.format.combine(
            winston.format.timestamp(),
            nestWinstonModuleUtilities.format.nestLike(),
          ),
        }),

        new DailyRotateFile({
          filename: '%DATE%.log',
          dirname: 'logs',
          datePattern: 'YYYY-MM-DD',
          maxSize: '1g',
          maxFiles: '30d',
          format: winston.format.combine(
            winston.format.timestamp(),
            nestWinstonModuleUtilities.format.nestLike(),
            winston.format.uncolorize(),
          ),
        }),
      ],
    }),
  });

  await app.listen(3000);

  if (module.hot) {
    module.hot.accept();
    module.hot.dispose(() => app.close());
  }
}
bootstrap();
