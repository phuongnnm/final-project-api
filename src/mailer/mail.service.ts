import { Injectable } from "@nestjs/common";
import { ConfigService } from "../config/config.service";
const nodemailer = require('nodemailer');
const hbs = require('nodemailer-express-handlebars');

@Injectable()
export class MailService {
    private transporter;

    constructor(private readonly configService: ConfigService) {
        this.transporter = nodemailer.createTransport({
            host: this.configService.get("MAILER_HOST"),
            port: this.configService.get("MAILER_PORT"),
            secure: false,
            tls: {
                ciphers: 'SSLv3'
            },
            requireTLS: true,
            auth: {
                user: this.configService.get("MAILER_USERNAME"),
                pass: this.configService.get("MAILER_PASSWORD")
            }
        });

        this.transporter.use('compile', hbs({
            viewEngine: {
                extName: '.hbs',
                partialsDir: 'src/mailer/templates',
                layoutsDir: 'src/mailer/templates',
                defaultLayout: 'index.hbs',
            },
            viewPath: 'src/mailer/templates',
            extName: '.hbs',
        }))
    }

    async sendMail(receivers: string[], subject: string, template: string, data: object) {
        const sentMail = await this.transporter.sendMail({
            from: this.configService.get("MAILER_USERNAME"),
            to: receivers.join(', '),
            subject,
            template,
            context: data
        }, ).catch(err => console.log(err));

        return sentMail;
    }

    async sendMailWithoutTemplate(receivers: string[], subject: string, htmlBody: string) {
        const sentMail = await this.transporter.sendMail({
            from: this.configService.get("MAILER_USERNAME"),
            to: receivers.join(', '),
            subject,
            html: htmlBody,
        });

        return sentMail;
    }
}