
import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { GqlExecutionContext } from '@nestjs/graphql';

@Injectable()
export class RolesGuard implements CanActivate {
    constructor(private readonly reflector: Reflector) { }
    canActivate(
        context: ExecutionContext,
    ): boolean {
        const ctx = GqlExecutionContext.create(context);
        const roles = this.reflector.get<string[]>("roles", context.getHandler()) || [];

        if (roles.length === 0) {
            return true;
        }
        const request = ctx.getContext().req;
        const user = request.user;

        return roles.findIndex(role => role === user.role) > -1;
    }
}
