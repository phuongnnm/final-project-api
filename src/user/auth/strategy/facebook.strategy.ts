import { use } from 'passport';
import { Injectable } from '@nestjs/common/decorators';
import * as FacebookTokenStrategy from 'passport-facebook-token';
import { ConfigService } from '../../../config/config.service';

@Injectable()
export class FacebookStrategy {
    constructor(private readonly configService: ConfigService) {
        this.init();
    }
    async init() {
        use(
            new FacebookTokenStrategy.default(
                {
                    clientID: this.configService.get('FB_CLIENT_ID'),
                    clientSecret: this.configService.get('FB_CLIENT_SECRET'),
                },
                async (
                    accessToken: string,
                    refreshToken: string,
                    profile: any,
                    done: any,
                ) => {
                    if (!profile._json.email) {
                        return done('Cấp email thất bại. Vui lòng gỡ ứng dụng trong "Ứng dụng và trang web" của Facebook rồi thử lại');
                    }

                    return done(null, {
                        ...profile._json,
                        avatarUrl: profile.photos[0].value,
                    });
                },
            ),
        );
    }
}
