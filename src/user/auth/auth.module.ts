import { FacebookStrategy } from './strategy/facebook.strategy';
import { AuthController } from './auth.controller';
import { Module, HttpModule } from '@nestjs/common';
import { UserModule } from '../user.module';
import { JwtModule } from '@nestjs/jwt';
import { ConfigModule } from '../../config/config.module';
import { ConfigService } from '../../config/config.service';

@Module({
    imports: [
        UserModule,
        HttpModule,
        JwtModule.registerAsync({
            imports: [ConfigModule],
            useFactory: async (configService: ConfigService) => ({
              secret: configService.get('JWT_SECRETKEY'),
              signOptions: { expiresIn: '9999 years' },
            }),
            inject: [ConfigService],
          }),
    ],
    providers: [FacebookStrategy],
    controllers: [AuthController],
})
export class AuthModule { }
