import { Role } from "../../models/roles";

export interface JwtPayload {
    email: string;
    username: string;
    id: string;
    hash: string;
    iat: number;
    exp: number;
    role: Role;
  }