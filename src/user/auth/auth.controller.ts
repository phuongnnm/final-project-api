import { Controller, UseGuards, Req, Post, Body, HttpService, HttpException, HttpStatus } from '@nestjs/common';
import { AuthGuard } from "@nestjs/passport";
import { UserService } from '../user.service';
import { RegisterType } from '../../models/registerType';
import { JwtService } from '@nestjs/jwt';

@Controller('auth')
export class AuthController {
    constructor(
        private readonly userService: UserService,
        private readonly jwtService: JwtService,
        private readonly httpService: HttpService,
    ) { }

    private getInfoSafety(value: string, replacedValue: string = 'Vkon User') {
        if (!value || !(value.trim().length >= 2 && value.trim().length <= 255)) {
            return replacedValue;
        }

        return value;
    }

    @UseGuards(AuthGuard('facebook-token'))
    @Post('login/facebook')
    async loginWithFacebookToken(@Req() req) {
       console.log(req);
        // return await this.userService.loginWithSocialMedia(req.user.email, RegisterType.FACEBOOK);
    }

    @UseGuards(AuthGuard('facebook-token'))
    @Post('register/facebook')
    async registerWithFacebookToken(@Req() req, @Body() additionalUserInfo) {
        try {
            let user = await this.userService.createUser({
                fullName: `${req.user.first_name} ${req.user.last_name}`,
                email: req.user.email,
                avatarUrl: req.user.avatarUrl,
                registerType: RegisterType.FACEBOOK
            });

            return {
                token: this.jwtService.sign({
                    email: user.email,
                    role: user.role,
                    id: user.id,
                }),
                user
            };
        } catch (err) {
            throw new Error(err);
        }
    }
}