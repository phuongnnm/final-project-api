import { Inject, Injectable, Scope } from '@nestjs/common';
import { CONTEXT } from '@nestjs/graphql';
import { JwtService } from '@nestjs/jwt';
import * as crypto from 'crypto';
import User from '../entity/user.entity';
import { RegisterType } from '../models/registerType';
import { Role } from '../models/roles';

@Injectable({ scope: Scope.REQUEST })
export class UserService {
    constructor(
        @Inject(CONTEXT) private readonly context,
        private readonly jwtService: JwtService,
    ) { }

    async createUser(userInfo): Promise<User> {
        const user = await User.create(userInfo as Object);
        return User.save(user);
    }

    async updateUser(userData: User): Promise<User> {
        return User.save(userData);
    }

    async deleteUser(id: string) {
        const user = await User.findOne(id);
        return User.remove(user);
    }

    async emailIsExisted(email) {
        const users = await User.find({
            where: [{ email, registerType: RegisterType.DEFAULT }],
        });
        return users.length > 0 ? true : false;
    }

    hashedPasswordWithSaltOrNot(plainPassword: string, salt: string = null) {
        let hashedPass = crypto.createHmac('sha256', plainPassword);

        if (plainPassword && salt) {
            hashedPass = hashedPass.update(salt);
        }

        return hashedPass.digest('hex');
    }

    async login(email: string, pass: string, isAdminSite: boolean = false): Promise<any> {
        const validateLogin = (user: User) => {
            if (isAdminSite) {
                if (user.role != Role.ADMIN) {
                    throw new Error('Unauthorized');
                }
            }

            const hashedPass = this.hashedPasswordWithSaltOrNot(pass, user.passwordSalt);

            if (!user || user.password !== hashedPass) {
                throw new Error('Unauthorized');
            }
        };

        const updatePasswordSalt = async (userData: User) => {
            if (!userData) {
                throw new Error('Unauthorized');
            }
            
            if (userData.password && !userData.passwordSalt) {
                userData.password = pass;
                await user.save();
            }
        };

        const user = await User
            .createQueryBuilder('user')
            .select('user.email')
            .addSelect('user.password')
            .addSelect('user.role')
            .addSelect('user.id')
            .addSelect('user.registerType')
            .addSelect('user.passwordSalt')
            .where(`user.email = '${email}'`)
            .getOne();

        validateLogin(user);
        updatePasswordSalt(user);

        return {
            token: this.jwtService.sign({
                email: user.email,
                role: user.role,
                id: user.id,
            }),
            user
        };
    }
}