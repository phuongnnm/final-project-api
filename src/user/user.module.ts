import { HttpModule, Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { ConfigService } from 'src/config/config.service';
import { ConfigModule } from '../config/config.module';
import { UserResolver } from '../graphql/resolvers/user';
import { FacebookStrategy } from './auth/strategy/facebook.strategy';
import { JwtStrategy } from './jwt.strategy';
import { UserService } from './user.service';

@Module({
  imports: [
    PassportModule.register({ defaultStrategy: 'jwt' }),
    JwtModule.registerAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) => ({
        secret: configService.get('JWT_SECRETKEY'),
        signOptions: { expiresIn: '9999 years' },
      }),
      inject: [ConfigService],
    }),
    HttpModule
  ],
  providers: [
    UserService,
    UserResolver,
    JwtStrategy,
  ],
  exports: [UserService]
})
export class UserModule {}
