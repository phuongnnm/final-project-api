import { ExtractJwt, Strategy } from 'passport-jwt';
import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { JwtPayload } from './auth/jwt-payload.interface';
import { PassportStrategy } from '@nestjs/passport';
import { ConfigService } from '../config/config.service';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(private readonly configService:ConfigService) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: configService.get('JWT_SECRETKEY'),
    });
  }

  async validate(payload: JwtPayload) {
    return {
      email: payload.email,
      username: payload.username,
      role: payload.role,
      id: payload.id,
    };
  }
}